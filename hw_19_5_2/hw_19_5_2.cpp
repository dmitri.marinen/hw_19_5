#include <iostream>
#include <string>

using namespace std;

class Animal
{
protected:
    string name;
public:
    string getName() {
        return name;
    }
    virtual void voice() {
        cout << "Animal Voice";
    }

    Animal(){}
    Animal(string _name) : name(_name) {}
};

class Dog : public Animal
{
private:

public:
    void voice() override {
        cout << this->getName()<< ": " << "Woof!\n";
    }

    Dog (){}
    Dog (string _name) : Animal(_name){}
};

class Cat : public Animal
{
private:

public:
    void voice() override {
        cout << this->getName() << ": " << "Meow!\n";
    }

    Cat(string _name) : Animal(_name) {}
};

class Mouse : public Animal
{
private:

public:
    void voice() override {
        cout << this->getName() << ": " << "Piu-piu!\n";
    }

    Mouse(){}
    Mouse(string _name) : Animal(_name) {}
};



int main()
{
    Animal* dog1 = new Dog("Bobik");
    Animal* cat1 = new Cat("Murzik");
    Animal* mouse1 = new Mouse("Pixel");

    Animal* animals[] = {dog1,cat1,mouse1};

    for (int i = 0; i < (sizeof(animals)/sizeof(animals[0])); i++) {
        animals[i]->voice();
    }

    return 0;
   
}